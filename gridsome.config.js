// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const path = require("path");

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/sass/*.scss")]
    });
}

module.exports = {
  siteName: "JB Delicatessen",
  plugins: [
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        tailwindConfig: "tailwind.config.js",
        purgeConfig: {
          whitelistPatternsChildren: [/aos/]
        },
        presetEnvConfig: {},
        shouldPurge: true,
        shouldImport: true,
        shouldTimeTravel: true
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Products",
        baseDir: "./site/content",
        path: "productos/*.md"
      }
    },
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Pages",
        baseDir: "./site/content",
        path: "*.md"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Categories",
        baseDir: "./site/content",
        path: "categorias/*.md"
      }
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "SalePoints",
        baseDir: "./site/content",
        path: "puntos-venta/*.md"
      }
    },
    {
      use: `gridsome-plugin-netlify-cms`
    }
  ],
  transformers: {
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: []
    }
  },
  templates: {
    Categories: "/categorias/:title"
  },
  chainWebpack(config) {
    // Load variables for all vue-files
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    types.forEach(type => {
      addStyleResource(config.module.rule("scss").oneOf(type));
    });
  }
};
