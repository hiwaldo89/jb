---
title: Butifarra Catalana
imagen: /uploads/jb-butifarra-catalana.jpg
tipo: Cerdo
icon: /uploads/butifarra-catalana.svg
caracteristicas: >-
  Carne de cerdo y huevo embutido en tripa natural, marcado por un delicado
  sabor a perejil.
consumo_recomendado: |-
  * Frío: En rebanadas para botana
  * Frita: Para hamburguesas, choripanes y emparedados

  Nota: Remover la tripa natural antes de su consumo.
vida_anaquel: |-
  * 4 días la rebanada
  * 2 semanas después de abierto
  * 1 año congelado
  * 3 meses empacado
maridaje: Texto
categoria: Butifarra
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/pan-icon.svg
---

