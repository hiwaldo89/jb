---
title: Jamón Campirano
imagen: /uploads/jb-jamon-campirano.jpg
tipo: Cerdo
icon: /uploads/jamon-icon.svg
caracteristicas: >-
  Jamón de pierna entera cocido y costreado en horno de ladrillo, obtiene su
  sabor con un delicado ahumado y miel de abeja natural.
consumo_recomendado: |-
  * Frío: Ideal para emparedados
  * Caliente: Rebanada gruesa bañada con salsa al gusto
vida_anaquel: |-
  * 1 semana la rebanada
  * 3 semanas después de abierto
  * 1 año congelado
  * 2 meses empacado
maridaje: Pan
categoria: Jamón
maridajes:
  - /uploads/salsa-icon.svg
  - /uploads/pan-icon.svg
---

