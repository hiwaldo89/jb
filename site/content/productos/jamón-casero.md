---
title: Jamón Casero
imagen: /uploads/jb-jamon-casero.jpg
tipo: Cerdo
icon: /uploads/jamon-casero.svg
caracteristicas: Pierna entera de cerdo embutida a mano y horneada en ladrillo.
consumo_recomendado: |-
  * Frío: Ideal para emparedados
  * Caliente: Rebanada gruesa bañada con salsa al gusto.
vida_anaquel: |-
  * 1 semana la rebanada
  * 3 semanas después de abierto
  * 1 año congelado 
  * 2 meses empacado
maridaje: Texto
categoria: Jamón
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/pan-icon.svg
  - /uploads/salsa-icon.svg
---

