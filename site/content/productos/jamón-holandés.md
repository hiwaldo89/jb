---
title: Jamón Holandés
imagen: /uploads/jb-jamon-holandes.jpg
tipo: Cerdo
icon: /uploads/jamon-holandes.svg
caracteristicas: Jamón de pierna entera cocido y condimentado a la usanza antigua.
consumo_recomendado: '* Frío: ideal para emparedados.'
vida_anaquel: |-
  * 1 semana la rebanada
  * 3 semanas después de abierto
  * 1 año congelado
  * 2 meses empacado
maridaje: Texto
categoria: Jamón
maridajes:
  - /uploads/salsa-icon.svg
  - /uploads/pan-icon.svg
---

