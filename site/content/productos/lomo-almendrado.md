---
title: Lomo Almendrado
imagen: /uploads/jb-lomo-almendrado.jpg
tipo: Cerdo
icon: /uploads/lomo-almendrado.svg
caracteristicas: >-
  Lomo de cerdo cocinado en horno de ladrillo y mechado con almendra, cebolla y
  tocino ahumado artesanal.
consumo_recomendado: >-
  * Frío / frito: Rebana delgada, ideal para emparedados

  * Caliente: Rebanada gruesa acompañada con salsa al gusto. (Salsas blancas
  como sugerencia)
vida_anaquel: |-
  * 1 semana la rebanada
  * 1 mes después de abierto
  * 1 año congelado
  * 4 meses empacado
maridaje: Texto
categoria: Lomo
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/pan-icon.svg
---

