---
title: Pierna Mechada
imagen: /uploads/jb-pierna-mechada.jpg
tipo: Cerdo
icon: /uploads/pierna-mechada.svg
caracteristicas: >-
  Pierna de cerdo horneada en ladrillo, adobada con chile ancho y mechada con
  almendra y pasa, integrando así el auténtico Jamón Virginia de antaño.
consumo_recomendado: >-
  * Frío / frita: Ideal para emparedados

  * Caliente: Rebanada gruesa bañada con salsa al gusto. (Jugo de piña como
  sugerencia)
vida_anaquel: |-
  * 4 días la rebanada
  * 3 semanas después de abierto
  * 1 año congelado 
  * 3 meses empacado
maridaje: Texto
categoria: Pierna
maridajes:
  - /uploads/salsa-icon.svg
  - /uploads/pan-icon.svg
---

