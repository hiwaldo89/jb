---
title: Queso de Puerco Artesanal
imagen: /uploads/jb-queso-de-puerco-artesanal.jpg
tipo: Cerdo
icon: /uploads/queso-puerco.svg
caracteristicas: >-
  Embutido a base de corte completo de cabeza de cerdo, aromatizado con hierbas
  finas con predominancia de orégano y elaborado en barricas de roble a la
  usanza tradicional mexicana.
consumo_recomendado: '* Frío: En rebanadas para emparedado o botana'
vida_anaquel: |-
  * 4 días la rebanada
  * 2 semanas después de abierto
  * 1 año congelado
  * 3 meses empacado
maridaje: Texto
categoria: Queso de puerco
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/salsa-icon.svg
---

