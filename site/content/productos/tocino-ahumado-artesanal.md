---
title: Tocino Ahumado Artesanal
imagen: /uploads/jb-tocino.ahumado-artesanal.jpg
tipo: Cerdo
icon: /uploads/tocino-ahumado.svg
caracteristicas: >-
  Panza de cerdo curada en seco (frotada a mano a la usanza tradicional) y
  ahumada con leña de roble.
consumo_recomendado: '* Versátil'
vida_anaquel: |-
  * 3 semanas después de abierto
  * 1 año congelado
  * 4 meses empacado
maridaje: Texto
categoria: Tocino
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/pan-icon.svg
  - /uploads/salsa-icon.svg
---

