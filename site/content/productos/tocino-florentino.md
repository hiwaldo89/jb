---
title: Tocino Florentino
imagen: /uploads/jb-tocino-florentino.jpg
tipo: Cerdo
icon: /uploads/tocino-florentino.svg
caracteristicas: >-
  Panza de cerdo enrollada con mecate, ligeramente horneada al estilo italiano
  con leña de roble y con un sabor intenso marcado principalmente por la semilla
  de alcaravea.
consumo_recomendado: '* Frío: Ideal para emparedados'
vida_anaquel: |-
  * 1 semana la rebanada
  * 1 mes después de abierto
  * 1 año congelado
  * 4 meses empacado
maridaje: Texto
categoria: Tocino
maridajes:
  - /uploads/vinos-icon.svg
  - /uploads/salsa-icon.svg
  - /uploads/pan-icon.svg
---

