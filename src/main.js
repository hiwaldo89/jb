// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import "~/main.css";
import "typeface-roboto";
import "~/assets/sass/styles.scss";
import "aos/dist/aos.css";
import DefaultLayout from "~/layouts/Default.vue";
//import VueScrollReveal from "gridsome-scroll-reveal";
import AOS from "aos";

export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
  //Vue.use(VueScrollReveal);
  // Vue.use(VueScrollReveal, {
  //   delay: 1
  // });
  //Vue.use(AOS);
  Vue.prototype.$AOS = AOS;
}
